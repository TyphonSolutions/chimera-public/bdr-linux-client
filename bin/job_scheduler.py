#!/usr/bin/env python
"""job_scheduler.py.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python job_scheduler.py
        $ ./job_scheduler.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * For module TODOs
    * Replace template docstring with valid content

"""
import time
import sched
from job_creator import enabled_jobs
from crontab import CronTab


def run_threaded(job_func):
    print("\n\nJOB STARTED:\nI'm running on thread %s" % threading.current_thread())
    print("I'm working on job " + job.name + " at " + str(time.asctime()))
    job_thread = threading.Thread(target=job_func)
    job_thread.start()


def run_job(job):
    print("\n\nJOB STARTED:\nI'm not threaded\n")
    print("I'm working on job " + job.name + " at " + str(time.asctime()))
    job.run()


def schedule_job(job):
    run_job(job)
    scheduler.enter(job.next_run.next(), 1, schedule_job, (job,))

scheduler = sched.scheduler(time.time, time.sleep)

for job in enabled_jobs:
    # only run 1st test job temporily
    if '01' in job.name:
        job.set_crontab("* * * * * *")
        job.next_run = CronTab(job.crontab)
        scheduler.enter(job.next_run.next(), 1, schedule_job, (job,))


while True:
    print(scheduler.queue)
    scheduler.run()
    time.sleep(60)
