#!/usr/bin/env python
"""job_creator.py.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python job_creator.py
        $ ./job_creator.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * For module TODOs
    * Replace template docstring with valid content

"""
import os
import ConfigParser
from rclone_cloud import Cloud
from backup_job import BackupJobConfigParser, BackupLogger

# create ConfigParser object
config = ConfigParser.ConfigParser()

# read in config from config files
master_config = 'config.ini'
config.read(master_config)
main_config = config.get('paths', 'main')
jobs_dir = config.get('paths', 'jobs')
config.read(main_config)
# read in job configs from jobs dir
for f in os.listdir(jobs_dir):
    if f.endswith(".ini"):
        config.read(os.path.join(jobs_dir, f))

# create logger object
logger = BackupLogger(config.get('logging', 'file'))
# create job_creator object
backup_job_creator = BackupJobConfigParser(config)
# empty list to hold all job instances
all_jobs = list()
# populate all_jobs
for section in config.sections():
    if section.startswith('job'):
        all_jobs.append(section)

# empty list to hold all enabled job instances
enabled_jobs = list()
# populate enabled_jobs
for job_name in all_jobs:
    job = backup_job_creator.create_job(job_name, logger=logger)
    if job.is_enabled():
        enabled_jobs.append(job)
